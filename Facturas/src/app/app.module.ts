import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FacturaComponent } from './contenido/factura/factura.component';
import { MenuComponent } from './contenido/menu/menu.component';
import { DetalleComponent } from './contenido/detalle/detalle.component';

@NgModule({
  declarations: [
    AppComponent,
    FacturaComponent,
    MenuComponent,
    DetalleComponent
  ],
  imports: [
    BrowserModule,
    NgbModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
